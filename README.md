## Table of Contents

* [Setup](#setup)
* [Available Scripts](#available-scripts)
  * [Start](#start)
  * [Test](#test)
  * [Lint](#lint)
* [Spec Documentation](#spec-documentation)
  * [Edit documentation](#Edit-Spec)
  * [View documentation](#View-Spec)
* [Jsdoc Documentation](#jsdoc-documentation)
  * [Edit documentation](#Edit-jsdoc)
  * [View documentation](#View-jsdoc)
* [Unit Test Coverage Report](#Unit-Test-Coverage-Report)

## Setup
Install dependencies
```
npm install
```

## Available Scripts
### start
```
npm run dev
```
Run the project

### Test
```
npm run test
```

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

### Lint
```
npm run lint
```

Run [eslint](https://github.com/eslint/eslint) utility to check whether the code format is correct.

### Generate jsdoc page
```
npm run jsdoc
```
Generate [jsdoc](http://usejsdoc.org/) page with Docdash [template](https://github.com/clenemt/docdash)

## Spec Documentation
### Edit Spec.

There are 2 ways to edit spec documentation:
1. Download [swagger editor](https://github.com/swagger-api/swagger-editor) and import /docs/swagger.yaml to edit the documentation. Don't forget to save after editing and replace previous one.

2. Install swagger viewer plugin on your IDE and edit with viewer.This plugin is for [VSCode](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer).
This plugin is for [Webstorm](https://plugins.jetbrains.com/plugin/8347-swagger-plugin).

Note: Please follow the [REST API style](https://www.restapitutorial.com/lessons/httpmethods.html) and [best practice]() to design or update documentation


### View Spec

You can refer to [online version](http://eab-digital-platform.eabgitlab-dc.com/development/cms-admin-backend/docs/index.html) (for only last commit on major branch)

## Jsdoc Documentation
### Edit jsdoc
Appropriate comments is better for us to understand the code more conveniently. We use the universal method: [jsdoc](http://usejsdoc.org/) and [Docdash template](https://github.com/clenemt/docdash) to view. Above each function and global variables, please add jsdoc for it. At lease, it should includes author, description. For convenience, recommend a plugin named [Add jsdoc comments](https://marketplace.visualstudio.com/items?itemName=stevencl.addDocComments) when you use VSCode for your IDE. Use [Auto create jsdoc comments](https://www.jetbrains.com/help/webstorm/creating-jsdoc-comments.html) when you use Webstorm for your IDE.

### View jsdoc
1. If you are in local environment, run ```npm run jsdoc``` and then it when generate jsdoc folder under the root folder. Open the ```index.html``` file inside, you will see the jsdoc page with [Docdash template](https://github.com/clenemt/docdash)

2. If you are in other environment, refer to [online version](http://eab-digital-platform.eabgitlab-dc.com/development/cms-admin-backend/jsdoc/index.html)(for only last commit on major branch)

## Unit Test Coverage Report
After `npm run test`, a coverage report will generate. It is on `./coverage/lcov-report` folder. You can click `index.html` to view the report.
