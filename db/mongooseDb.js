import mongoose from 'mongoose';

const config = require('../config/config.json');

const {
  username, password, host, port, database,
} = config.mongodb;

const DB_URL = `mongodb://${username}:${password}@${host}:${port}/${database}`;

mongoose.Promise = global.Promise;
/**
 * 连接
 */
mongoose.connect(DB_URL);

/**
  * 连接成功
  */
mongoose.connection.on('connected', () => {
  // eslint-disable-next-line no-console
  console.log(`Mongoose connection success to ${DB_URL}`);
});

/**
 * 连接异常
 */
mongoose.connection.on('error', (err) => {
  // eslint-disable-next-line no-console
  console.log(`Mongoose connection error: ${err}`);
});

/**
 * 连接断开
 */
mongoose.connection.on('disconnected', () => {
  // eslint-disable-next-line no-console
  console.log('Mongoose connection disconnected');
});

export default mongoose;
