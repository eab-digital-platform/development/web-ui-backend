# FROM registry.redhat.io/rhoar-nodejs/nodejs-10
#FROM node
FROM node:12.2.0-alpine
EXPOSE 3001:3001
#ENV NODE_ENV production
#ENV CONFIG PRO
#ENV DEV_MODE false
#USER 0
WORKDIR /app
COPY . /app
RUN npm install --loglevel error
#RUN npm run start
CMD ["npm", "run", "dev"]
