import express from 'express';
import indexController from '../controller/index.controller';
import PageJson from '../models/PageJson';

const router = express.Router();

router.get('/', (req, res) => {
  indexController.homePage(req, res);
});

router.get('/test', (req, res) => {
  indexController.homePage2(req, res);
});

router.get('/test', (req, res) => {
  indexController.homePage2(req, res);
});


router.get('/api/getJsonByDocId/:docId', async (req, res) => {
  const pageJson = await PageJson.findOne({ id: req.params.docId });
  return res.status(200).send(pageJson);
});

export default router;
