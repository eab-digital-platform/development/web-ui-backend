import mongoose from '../../db/mongooseDb';

const { Schema } = mongoose;

const anySchema = new Schema({
  id: String,
  type: String,
  pageOrComponent: String,
  name: String,
},
{ collection: 'pageJson' });

const PageJson = mongoose.model('PageJson', anySchema);

export default PageJson;
