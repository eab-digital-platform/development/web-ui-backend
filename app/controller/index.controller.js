/**
 * @module controller/index
 */
import log4jUtil from '../utils/log4j.util';

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Test home page
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @returns {string} Hello World
 */
const homePage = (req, res) => {
  log4jUtil.log('info', 'Hello World 6');
  res.send('Hello World 6');
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Test home page (second one)
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @returns {string} Hello World 2
 */
const homePage2 = (req, res) => {
  log4jUtil.log('warn', 'Hello World 2');
  res.send('Hello World 2');
};

export default {
  homePage2,
  homePage,
};
