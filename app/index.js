/**
 * @module app/index
 */

import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import cors from 'cors';
import log4js from 'log4js';
import helmet from 'helmet';

import indexRouter from './routes/index.route';
import testRouter from './routes/test.route';

const app = express();

const log4j = log4js.getLogger();
log4j.level = 'debug';

app.use(cors());

// Init Helmet security lib
// Included features by default:
// 1. dnsPrefetchControl(controls browser DNS prefetching),
// 2. frameguard(prevent clickjacking),
// 3. hidePoweredBy (remove the X-Powered-By header),
// 4. hsts (HTTP Strict Transport Security),
// 5. ieNoOpen (sets X-Download-Options for IE8+)
// 6. noSniff (keep clients from sniffing the MIME type)
// 7. xssFilter(adds some small XSS protections)
// ref to: https://helmetjs.github.io/docs/
app.use(helmet());
app.use(helmet.noCache());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/abc', testRouter);

export default app;
